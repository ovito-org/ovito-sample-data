from pathlib import Path
import mimetypes


def is_text(fname):
    text_characters = bytes(range(32, 127)) + b"\n\r\t\b"
    max_non_text = 0.30

    with open(fname, "rb") as f:
        data = f.read(4 * 1024)
        if not data:
            return True

        non_text = sum(1 for byte in data if byte not in text_characters)
        if non_text / len(data) > max_non_text:
            return False
        return True


def refresh_file(file_suffix):
    attr_file = Path(".gitattributes")
    assert attr_file.is_file()

    with open(attr_file, "r") as f:
        curr_attr_file = f.readlines()

    with open(attr_file, "w") as f:
        for line in curr_attr_file:
            if line.strip() == "#### Auto generated section ####":
                f.write(line)
                for suffix in file_suffix:
                    f.write(f"{suffix.as_posix()} text eol=lf\n")
                return
            else:
                f.write(line)


def convert_line_endings(fname):
    windows_line_ending = b"\r\n"
    unix_line_ending = b"\n"
    with open(fname, "rb") as f:
        current_file = f.read()

    if windows_line_ending not in current_file:
        return

    updated_file = current_file.replace(windows_line_ending, unix_line_ending)
    with open(fname, "wb") as f:
        f.write(updated_file)


def main():
    files = []
    for dirpath, dirnames, filenames in Path.cwd().walk():
        if dirpath == Path(r"D:\Work\ovito-pro\ovito\tests\files"):
            continue
        for fname in filenames:
            if fname.startswith("."):
                continue
            if not is_text(Path(dirpath, fname)):
                continue
            if Path(fname) == Path("animation.windows_encoding.dump"):
                continue
            convert_line_endings(Path(dirpath, fname))
            files.append(Path(dirpath, fname).relative_to(Path.cwd()))
    files.sort()
    refresh_file(files)


if __name__ == "__main__":
    main()
